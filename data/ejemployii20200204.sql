﻿--
-- Script was generated by Devart dbForge Studio 2019 for MySQL, Version 8.1.45.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 10/02/2020 10:41:50
-- Server version: 5.5.5-10.1.40-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE ejemployii20200204;

--
-- Drop table `alumnos`
--
DROP TABLE IF EXISTS alumnos;

--
-- Set default database
--
USE ejemployii20200204;

--
-- Create table `alumnos`
--
CREATE TABLE IF NOT EXISTS alumnos (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) NOT NULL,
  apellidos varchar(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 8,
AVG_ROW_LENGTH = 2340,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

-- 
-- Dumping data for table alumnos
--
INSERT INTO alumnos VALUES
(1, 'Ramon', 'Perez'),
(2, 'Ramon', 'Perez'),
(3, 'Jorge', 'alvarez'),
(4, 'laura', 'gutierrez'),
(5, 'silvia', 'lopez'),
(6, 'laura', 'gutierrez'),
(7, 'silvia', 'lopez');

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;